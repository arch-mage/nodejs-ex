const express = require('express')
const morgan = require('morgan')

const app = express()

app.use(morgan('dev'))

const PORT = process.env.PORT || process.env.OPENSHIFT_NODEJS_PORT || 5000
const IP = process.env.IP || process.env.OPENSHIFT_NODEJS_IP || '0.0.0.0'

app.listen(PORT, IP)
